import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BillList extends StatefulWidget {
  @override
  _BillListState createState() => _BillListState();
}

class _BillListState extends State<BillList> {
  var _yearBill = ['2562', '2563', '2564'];

  var _monthBill = [
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฏาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม'
  ];

  var _currentYearBill;
  var _currentMonthBill = 'มกราคม';

  @override
  void initState() {
    super.initState();
    var yearNow = DateTime.now().year + 543;
    _currentYearBill = yearNow.toString();
    var monthNow = DateTime.now().month;
    // print(_monthBill[monthNow - 1]);
    _currentMonthBill = _monthBill[monthNow - 1];
  }

  Future<bool> onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.grey[200],
        title: Text("ยืนยันการออกจากระบบ"),
        content: Text('คุณต้องการออกจากระบบ Winner Bill ใช่หรือไหม?'),
        actions: <Widget>[
          OutlineButton(
            child: Text(
              "ยกเลิก",
              style: TextStyle(
                fontFamily: 'Prompt',
                color: Colors.grey[800],
              ),
            ),
            onPressed: () => Navigator.pop(context, false),
          ),
          RaisedButton(
            color: Colors.grey[800],
            child: Text(
              "ออกจากระบบ",
              style: TextStyle(
                fontFamily: 'Prompt',
                color: Colors.grey[200],
              ),
            ),
            onPressed: () => Navigator.pop(context, true),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[800],
          title: Text(
            'Winner Bill',
            style: TextStyle(fontFamily: 'Prompt'),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(FontAwesomeIcons.plus),
              onPressed: () {
                Navigator.of(context).pushNamed('billAdd');
              },
            ),
            IconButton(
              icon: Icon(FontAwesomeIcons.shareAlt),
              onPressed: () {
                print('share');
              },
            ),
            IconButton(
              icon: Icon(FontAwesomeIcons.signOutAlt),
              onPressed: onBackPressed,
            )
          ],
        ),
        body: Container(
          child: Stack(
            children: <Widget>[
              bgshow(),
              selectOption(context),
              totalDiv(context),
              listData(),
            ],
          ),
        ),
      ),
    );
  }

  Widget bgshow() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/bg.jpg'),
          colorFilter: new ColorFilter.mode(
            Colors.white.withOpacity(0.55),
            BlendMode.dstATop,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget selectOption(context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          yearDropdown(context),
          monthDropdown(context),
        ],
      ),
    );
  }

  Widget yearDropdown(context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.only(left: 10, right: 10),
      width: MediaQuery.of(context).size.width / 2 - 16,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'ปี',
            style: TextStyle(
                color: Colors.grey[200], fontFamily: 'Prompt', fontSize: 16),
          ),
          Container(
            width: 110,
            child: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: Colors.grey[800],
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  items: _yearBill.map((String dropDownStringItem) {
                    return DropdownMenuItem<String>(
                      value: dropDownStringItem,
                      child: Text(dropDownStringItem),
                    );
                  }).toList(),
                  onChanged: (String newValueSelected) {
                    setState(() {
                      _currentYearBill = newValueSelected;
                    });
                  },
                  value: _currentYearBill,
                  style: TextStyle(
                    fontFamily: 'Prompt',
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget monthDropdown(context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.only(left: 10, right: 10),
      width: MediaQuery.of(context).size.width / 2 - 16,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'เดือน',
            style: TextStyle(
                color: Colors.grey[200], fontFamily: 'Prompt', fontSize: 16),
          ),
          Container(
            width: 110,
            child: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: Colors.grey[800],
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  items: _monthBill.map((String dropDownStringItem) {
                    return DropdownMenuItem<String>(
                      value: dropDownStringItem,
                      child: Text(dropDownStringItem),
                    );
                  }).toList(),
                  onChanged: (String newValueSelected) {
                    setState(() {
                      _currentMonthBill = newValueSelected;
                    });
                  },
                  value: _currentMonthBill,
                  style: TextStyle(
                    fontFamily: 'Prompt',
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget totalDiv(context) {
    return Container(
      margin: EdgeInsets.only(top: 60, left: 8, right: 8),
      width: MediaQuery.of(context).size.width - 16,
      padding: EdgeInsets.all(10),
      height: 50,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'ยอดรวม',
                style: TextStyle(
                  fontFamily: 'Prompt',
                  color: Colors.grey[200],
                  fontSize: 18,
                ),
              ),
              Text(
                '16,265.25 บาท',
                style: TextStyle(
                  fontFamily: 'Prompt',
                  color: Colors.grey[200],
                  fontSize: 18,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget listData() {
    return Padding(
      padding: EdgeInsets.only(top: 120, left: 8, right: 8),
      child: ListView(
        children: <Widget>[
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
          listDetail('2', 'ค่าน้ำคอนโด', 'นิติบุคคลศุภาลัย', '255.28'),
        ],
      ),
    );
  }

  Widget listDetail(datebill, description, payfor, cost) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: 8),
          width: double.infinity,
          height: 60,
          decoration: BoxDecoration(
            color: Colors.grey[800],
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 8, left: 50),
          width: double.infinity,
          height: 60,
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        Container(
          width: 50,
          height: 60,
          child: Center(
            child: Text(
              datebill,
              style: TextStyle(
                fontFamily: 'Prompt',
                color: Colors.grey[200],
                fontSize: 24,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            left: 70,
            top: 5,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                description,
                style: TextStyle(
                  fontFamily: 'Prompt',
                  fontSize: 18,
                ),
              ),
              Text(
                payfor,
                style: TextStyle(
                  fontFamily: 'Prompt',
                  fontSize: 14,
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            right: 8,
          ),
          height: 60,
          alignment: Alignment(1, 0),
          child: Text(
            cost,
            style: TextStyle(
              fontFamily: 'Prompt',
              fontSize: 18,
            ),
          ),
        )
      ],
    );
  }
}
