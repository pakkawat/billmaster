import 'package:billmaster/billAdd.dart';
import 'package:billmaster/billList.dart';
import 'package:billmaster/login.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      routes: <String, WidgetBuilder>{
        'login': (context) => LoginPage(),
        'billList': (context) => BillList(),
        'billAdd': (context) => BillAdd()
      },
    );
  }
}
