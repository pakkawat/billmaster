import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BillAdd extends StatefulWidget {
  @override
  _BillAddState createState() => _BillAddState();
}

class _BillAddState extends State<BillAdd> {
  final format = DateFormat("dd-MM-yyyy");

  final formKey = GlobalKey<FormState>();
  final db = Firestore.instance;

  List<String> tempdata;

  Map<String, dynamic> dataInput = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่ม Bill'),
        backgroundColor: Colors.grey[800],
      ),
      body: Form(
        key: formKey,
        child: Stack(
          children: <Widget>[
            bgshow(),
            inputDiv(),
          ],
        ),
      ),
    );
  }

  Widget bgshow() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/bg.jpg'),
          colorFilter: new ColorFilter.mode(
            Colors.grey.withOpacity(0.6),
            BlendMode.dstATop,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget inputDiv() {
    return Container(
      margin: EdgeInsets.all(8),
      width: double.infinity,
      height: 315,
      decoration: BoxDecoration(
        color: Colors.grey[800].withOpacity(.9),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: <Widget>[
          row1(),
          row2(),
          row3(),
          row4(),
          savebtn(),
        ],
      ),
    );
  }

  Widget row1() {
    return Container(
      padding: EdgeInsets.only(
        left: 8,
        top: 8,
        right: 8,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: 16,
              top: 8,
            ),
            width: 100,
            child: Text(
              'วันที่',
              style: TextStyle(
                fontFamily: 'Prompt',
                fontSize: 16,
                color: Colors.grey[200],
              ),
            ),
          ),
          datePicker(),
        ],
      ),
    );
  }

  Widget datePicker() {
    return Container(
      width: 250,
      child: DateTimeField(
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
          ),
        ),
        format: format,
        style: TextStyle(
          color: Colors.grey[200],
        ),
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime(2019),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2025));
          return date;
        },
        onSaved: (DateTime value) {
          dataInput['dateInput'] = value.toString();
        },
      ),
    );
  }

  Widget row2() {
    return Container(
      padding: EdgeInsets.only(
        left: 8,
        top: 8,
        right: 8,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: 16,
              top: 8,
            ),
            width: 100,
            child: Text(
              'ผู้รับเงิน',
              style: TextStyle(
                fontFamily: 'Prompt',
                fontSize: 16,
                color: Colors.grey[200],
              ),
            ),
          ),
          Container(
            width: 250,
            child: TextFormField(
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]),
                ),
              ),
              style: TextStyle(
                color: Colors.grey[200],
                fontFamily: 'Prompt',
                fontSize: 16,
              ),
              onSaved: (String value) {
                dataInput['payfor'] = value;
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget row3() {
    return Container(
      padding: EdgeInsets.only(
        left: 8,
        top: 8,
        right: 8,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: 16,
              top: 8,
            ),
            width: 100,
            child: Text(
              'รายละเอียด',
              style: TextStyle(
                fontFamily: 'Prompt',
                fontSize: 16,
                color: Colors.grey[200],
              ),
            ),
          ),
          Container(
            width: 250,
            child: TextFormField(
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]),
                ),
              ),
              style: TextStyle(
                color: Colors.grey[200],
                fontFamily: 'Prompt',
                fontSize: 16,
              ),
              onSaved: (String value) {
                dataInput['detail'] = value;
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget row4() {
    return Container(
      padding: EdgeInsets.only(
        left: 8,
        top: 8,
        right: 8,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: 16,
              top: 8,
            ),
            width: 100,
            child: Text(
              'จำนวนเงิน',
              style: TextStyle(
                fontFamily: 'Prompt',
                fontSize: 16,
                color: Colors.grey[200],
              ),
            ),
          ),
          Container(
            width: 250,
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]),
                ),
              ),
              style: TextStyle(
                color: Colors.grey[200],
                fontFamily: 'Prompt',
                fontSize: 16,
              ),
              onSaved: (String value) {
                if (isNumeric(dataInput['price'])) {
                  dataInput['price'] = double.parse(value);
                } else {
                  dataInput['price'] = 0;
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget savebtn() {
    return Container(
      margin: EdgeInsets.only(
        top: 32,
      ),
      width: 300,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Text(
          'บันทึก',
          style: TextStyle(
            fontFamily: 'Prompt',
            fontSize: 18,
          ),
        ),
        onPressed: () {
          saveData();
        },
      ),
    );
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  void saveData() async {
    formKey.currentState.save();

    tempdata = dataInput['dateInput'].split("-");
    dataInput['yearInput'] = int.parse(tempdata[0].toString());
    dataInput['monthInput'] = int.parse(tempdata[1].toString());
    tempdata = tempdata[2].split(" ");
    dataInput['dayInput'] = int.parse(tempdata[0]);
    await db.collection("billList").add(dataInput).then((res) {
      Navigator.of(context).pushNamed('billList');
    });
  }
}
