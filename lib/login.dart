import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool showPassword = true;
  // final db = Firestore.instance;

  void changeStatePassword() {
    setState(() {
      showPassword = !showPassword;
    });
  }

  void checkUserPasswrod() async {
    // await db.collection("test").add({
    //   'title': 'Mastering Flutter',
    //   'description': 'Programming Guide for Dart'
    // });
    Navigator.of(context)
        .pushNamedAndRemoveUntil('billList', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            bgLogin(),
            boxLogin(context),
          ],
        ),
      ),
    );
  }

  Widget bgLogin() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/bg.jpg'),
          colorFilter: new ColorFilter.mode(
            Colors.black.withOpacity(0.8),
            BlendMode.dstATop,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget boxLogin(context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        height: 400,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Color.fromRGBO(200, 200, 200, 0.8),
        ),
        child: Column(
          children: <Widget>[
            titleText(),
            lineTitle(),
            desTitle(),
            inputLoginDiv(context),
            btnLogin(),
          ],
        ),
      ),
    );
  }

  Widget titleText() {
    return Container(
      margin: EdgeInsets.only(top: 24),
      child: Text(
        "Winner Bill",
        style: TextStyle(
          color: Colors.grey[800],
          fontSize: 50,
          fontFamily: 'Prompt',
        ),
      ),
    );
  }

  Widget lineTitle() {
    return Container(
      color: Colors.grey[800],
      width: 300,
      height: 3,
    );
  }

  Widget desTitle() {
    return Container(
      margin: EdgeInsets.only(top: 8),
      child: Text(
        'Flutter - FireStore Project',
        style: TextStyle(
          fontFamily: 'Prompt',
          fontSize: 20,
          color: Colors.grey[800],
        ),
      ),
    );
  }

  Widget inputLoginDiv(context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: MediaQuery.of(context).size.width - 40,
      height: 170,
      decoration: BoxDecoration(
        color: Colors.grey[800].withOpacity(0.8),
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 10),
        child: Column(
          children: <Widget>[
            phoneNumberInput(),
            passwordInput(),
          ],
        ),
      ),
    );
  }

  Widget phoneNumberInput() {
    return TextFormField(
      style: TextStyle(
        fontFamily: 'Prompt',
        color: Colors.grey[200],
      ),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey[200],
          ),
        ),
        icon: Icon(
          FontAwesomeIcons.mobileAlt,
          color: Colors.grey[300],
          size: 36,
        ),
        labelText: 'เบอร์โทรศัพท์',
        labelStyle: TextStyle(
          fontFamily: 'Prompt',
          color: Colors.grey[300],
        ),
      ),
    );
  }

  Widget passwordInput() {
    return TextFormField(
      obscureText: showPassword,
      style: TextStyle(
        fontFamily: 'Prompt',
        color: Colors.grey[200],
      ),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey[200],
          ),
        ),
        suffixIcon: IconButton(
          icon: showPassword
              ? Icon(
                  FontAwesomeIcons.eye,
                  color: Colors.grey[200],
                )
              : Icon(
                  FontAwesomeIcons.eyeSlash,
                  color: Colors.grey[200],
                ),
          onPressed: changeStatePassword,
        ),
        icon: Icon(
          FontAwesomeIcons.key,
          color: Colors.grey[300],
          size: 30,
        ),
        labelText: 'รหัสผ่าน',
        labelStyle: TextStyle(
          fontFamily: 'Prompt',
          color: Colors.grey[300],
        ),
      ),
    );
  }

  Widget btnLogin() {
    return Container(
      margin: EdgeInsets.only(
        top: 17,
      ),
      width: 300,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Text(
          'เข้าสู่ระบบ',
          style: TextStyle(
            fontFamily: 'Prompt',
            fontSize: 18,
          ),
        ),
        onPressed: () {
          checkUserPasswrod();
        },
      ),
    );
  }
}
